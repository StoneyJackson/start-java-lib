package lib;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
// import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class LibraryTest {
    @Test
    void testSomeLibraryMethod() {
        Library classUnderTest = new Library();
        Boolean result = classUnderTest.someLibraryMethod();
        assertThat(result).as("someLibraryMethod should return 'true'").isTrue();
        // assertTrue(classUnderTest.someLibraryMethod(), "someLibraryMethod should return 'true'");
    }
}
