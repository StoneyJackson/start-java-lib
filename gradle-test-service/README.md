# Gradle Test Service

_A docker-based, test service for Gradle-based projects._

As a developer on a team, I want a Docker container with Gradle in it to continuously run unit tests on my local Gradle-based project, so that (1) I get all the benefits of using my local IDE while rapidly receiving test results from a stable test environment, (2) I can work on different Gradle-based projects that may use different versions of Java and/or Gradle without them interfering with each other, and (3) our team won't suffer from the works-on-my-machine syndrome.

## Requires

* Docker
* A Gradle project

## Installing

Clone this project into your Gradle-based project's root directory. So from your project's root.

```bash
git clone [THIS_PROJECT]
```

Remove the `./gradle-test-service/.git` directory.

```bash
rm -rf ./gradle-test-service/.git
```

## Configuring

Set the version of Gradle and Java used by the service by editing the FROM statement in `./gradle-test-service/Dockerfile`. See [Gradle on Dockerhub](https://hub.docker.com/_/gradle) for possible tags.

## Running

Run the service from the root of your gradle-based project.

```bash
docker-compose -f ./gradle-test-service/docker-compose.yml up
```

This command will not return. It will continue compiling and running your project's tests whenever a file is changes.

Stop the service by pressing CTRL+C in the same terminal in which you started the service.

## Cleaning

If you won't be running the service again anytime soon, delete the Docker container and images.

```bash
docker system prune
docker images          # identify gradle-test-service images
docker rmi IMAGE_ID_1 IMAGE_ID_2 ...
```

## How it works

`gradle-test-service/Dockerfile` creates a Docker image containing Gradle configured to run `gradle test --info --continuous` on the project in its `/home/gradle/project` directory. `gradle-test-service/docker-compose.yml` runs a container based on the Gradle Docker image, mounting the root of the Gradle project to `/home/gradle/project` in the container. That's about it.


## References

- The works-on-my-machine syndrome.
  <https://www.leadingagile.com/2017/03/works-on-my-machine/>
- Using Docker for development.
  <https://docs.docker.com/develop/>
- Using Gradle Docker image to builds a project.
  <https://codefresh.io/docs/docs/learn-by-example/java/gradle/>
- Gradle images on Dockerhub.
  <https://hub.docker.com/_/gradle>
