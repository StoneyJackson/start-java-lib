# Start Java Lib

## Features

- JUnit 5
- AssertJ
- Mockito
- Gradle Test Service (see ./gradle-test-service/README.md)

## Running Tests

In the project root,

```bash
docker-compose -f ./gradle-test-service/docker-compose.yml up
```

CTRL+C to stop.
